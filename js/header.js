import { $, elems } from './DOMElems.js';

const checkAuth = () => {
    if (localStorage.hasOwnProperty('userToken')){
        $(elems.header.signInButton).style.display = "none";
        $(elems.header.createNewVisitButton).style.display = "block";
    }
}
export {
    checkAuth
}