import { checkAuth } from './header.js';
import { addAuthModalToHTML } from './authModal.js';
import { addCreateVisitModalToHTML } from './createVisitModal.js';
import { getAllCards } from './createCardModal.js'
import { deleteCard } from './deleteCard.js'
import { filterCards } from './filterCards.js'
import { showDetails } from './cardDetails.js'
import { editCard } from './editCardModal.js';
import { changeStatusCard } from './changeStatusCard.js';

checkAuth();
addAuthModalToHTML();
addCreateVisitModalToHTML();
getAllCards();
deleteCard();
filterCards();
showDetails();
changeStatusCard();
editCard();