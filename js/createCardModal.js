import { BASE_URL } from './constants.js';
import { VisitCardiologist, VisitDentist, VisitTherapist } 
from './childrenClasses.js'
import {$, elems} from './DOMElems.js'


const getAllCards = async function () {
   $('.cards-list__wrapper').innerHTML = '';
   const token = localStorage.getItem('userToken');
   try {
      fetch(`${BASE_URL}`, {
         method: 'GET',
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
         }
      })
      .then((res) =>{
        
         if (res.status === 200){
           return res.json();
         }
         else{
            throw new Error("Не корректный ответ");
         }
      })
      .then((res) =>{
         if(res.length === 0) {
            $('.cards-list__wrapper').innerHTML = '<span class="cards-list__empty">Запланированных визитов еще нет</span>';
            return;
         }
        const examplares = res.map(el => (el.doctor ===  "cardiologist")? new VisitCardiologist(el) : (el.doctor === "therapist") ?  new VisitTherapist(el) :  new VisitDentist(el))
        return examplares
      })
      .then((res) =>{
        const mainDiv = document.createElement('div');
        mainDiv.classList.add('row', 'row__cards');
        mainDiv.append(...res.map(rate =>{
         const div = document.createElement('div');
         div.classList.add('col-md-6')
         div.innerHTML = rate.render()
          return div
        }))
        const wrapper = $('.cards-list__wrapper');
        wrapper.append(mainDiv);
      
      })

   }
   catch (err) {
      console.log(err);
   }
}


export {
   getAllCards
}
