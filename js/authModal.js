import { Modal } from './mainClasses.js';
import { $, elems } from './DOMElems.js';
import { BASE_URL } from './constants.js';
import { closeModal } from './helpers.js';

const getErrorMsg = (msg) => {
    return `
    <div class="alert alert-danger auth-modal-error" role="alert">
        ${msg}
    </div>
    `
}

const login = (email, password) => {
    $(elems.auth.errorMessage) ? $(elems.auth.errorMessage).remove() : null;
    try {
        fetch(`${BASE_URL}/login`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: email, password: password })
          })
        .then((res) => {
            if(res.status === 200) {
                return res.text();
            }
            else if(res.status === 401) {
                throw new Error('Неправильный email или пароль');
            }
            else {
                throw new Error('Ответ сети был не ok.');
            
            }
        })
        .then((res) => {
            localStorage.setItem('userToken', res);
            closeModal($(elems.auth.authModal));
            window.location.reload(false); 
        })
        .catch(err => {
            $(elems.auth.modalBody).insertAdjacentHTML('afterbegin', getErrorMsg(err.message));
        })
    }
    catch(err) {
        console.log(err);
    };

} 

const onSubmitHandler = (e) => {
    e.preventDefault();
    
    const email = $(elems.auth.authEmailInput).value;
    const password = $(elems.auth.authPasswordInput).value;

    login(email, password);
}

const addEventListenerToSignInBtn = () => {
    $(elems.auth.submitButton).addEventListener('click', e => onSubmitHandler(e))
}

const addAuthModalToHTML = () => {
    const propsObj = {
        id: "authModal",
        title: "Войти в систему",
        type: "authModal",
        action: '',
        submitBtnText: "Войти"
    };
    
    const authModal = new Modal(propsObj);
    
    $(elems.main).insertAdjacentHTML('beforebegin', authModal.getHtmlToInsert());

    addEventListenerToSignInBtn();
}

export {
    addAuthModalToHTML
}