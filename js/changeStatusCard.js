const  openStatus =(event) =>{
   const info = event.target.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[5].childNodes[1].innerText;
   if (info !== 'done'){
      return true
   }
   alert('Статус уже выполнен');
}
const beChanged = () => {
   if (!confirm("Поменять статус на выполнен?")) {
      return
   }
   return true
}

const getUserInfo = (event) => {
   const card = event.target.parentNode.parentNode;
   const id = +card.id;
   const info = card.childNodes[1].childNodes[1];
   const userInfo = info.childNodes[1].childNodes[1].firstChild.innerText.split(' ');
   const name = userInfo[1];
   const surname = userInfo[0];
   const patronim = userInfo[2];
   const userDoctor = info.childNodes[3].childNodes[1].firstChild.innerText;
   return {
      id,
      name,
      surname,
      patronim,
      userDoctor
   }
}

const invalidBtn = (event) =>{
   const btn = event.target.previousElementSibling.previousElementSibling;
   btn.setAttribute('disabled', true);
}

const changeStatusCard = () => {
   document.body.addEventListener('click', function (event) {
      if (event.target.classList.contains('change-status-button')) {
         if(openStatus(event)){
            if (beChanged()) {
               invalidBtn(event);
               const token = localStorage.getItem('userToken');
               const { id,
                  name,
                  surname,
                  patronim,
                  userDoctor } = getUserInfo(event);
               try {
                  fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                     method: 'PUT',
                     headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                     },
                     body: JSON.stringify({
                        id: id,
                        status: 'done',
                        name: name,
                        surname: surname,
                        patronim: patronim,
                        doctor: userDoctor
                     })
                  }) .then(() => {
                     window.location.reload(false); 
                 })
               }
               catch (err) {
                  console.log(err);
               }
            }
         }
      }
      return
   }
   )
}
export {
   changeStatusCard
}