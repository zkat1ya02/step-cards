import { BASE_URL } from './constants.js';

export const fetchAllCards = (token) => {
    return fetch(`${BASE_URL}`, {
         method: 'GET',
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
         }
    })
}

export const fetchCardById = (token, cardId) => {
    return fetch (`${BASE_URL}/${cardId}`,{
        method: 'GET',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
}

export const updateCard = (token, cardId, updatedInfo) => {
    return fetch (`${BASE_URL}/${cardId}`, {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
        },
        body: updatedInfo
    })
}

export const deleteCard = (token, cardId) => {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
}