class Modal {
    constructor(params){
        this.id = params.id;
        this.title = params.title;
        this.type = params.type;
        this.submitBtnText = params.submitBtnText;
        this.action = params.action
    }

    getHtmlToInsert() {
        const id = this.id;
        const title = this.title;
        const type = this.type;
        const action = this.action;
        const submitBtnText = this.submitBtnText;

        switch(type){
            case 'infoModal': 
                return ` <div class="modal fade" id="${id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">${title}</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <p>Доктор: <span class="doctor-visit"><b>терапевт</b></span></p>
                        </div>
                        <div>
                            <p>Цель визита: <span class="title-visit"><b>плановый осмотр</b></span></p>
                        </div>
                        <div>
                            <p>Краткое описание визита: <span class="description-visit"><b>плановый осмотр</b></span></p>
                        </div>
                        <div>
                            <p>Фамилия: <span class="surname-visit"><b>Шеварнадзе</b></span></p>
                        </div>
                        <div>
                            <p>Имя: <span class="name-visit"><b>Ирина</b></span></p>
                        </div>
                        <div>
                            <p>Отчество: <span class="patronim-visit"><b>Георгиевна</b></span></p>
                        </div>
                        <div class="place-to-render-inputs place-to-render-inputs-info-modal">
                        
                        </div>
                    </div>
                </div>
              </div>
               </div>`;
            case 'authModal':
                return ` <div class="modal fade" id="authModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">${title}</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                     </div>
                     <div class="modal-body-auth">
                       <form action="" class="auth-form">
                         <div class="modal__item">
                             <label for="email">Email:</label>
                             <input type="text" class="auth-email-input" id="email" name="email" required>
                         </div>
                         
                         <div class='modal__item'>
                             <label for="password">Password:</label>
                             <input type="password" class="auth-password-input" id="password" name="password" required>
                         </div>
                         
                         <input class="auth-form-submit-button form-button btn btn-primary btn-lg" type="submit" value="${submitBtnText}">
                       </form>
                     </div>
                   </div>
              </div>
              
              </div>`;
            case 'visitModal': 
                return ` <div class="modal fade" id="${id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">${title}</h5>
                       <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                     </div>
                     <div class="modal-body-${action}">
                       <form action="" class="${action}-visit-form">
                       <div  class="modal__item">
                           <label for="doctor">Выберите доктора</label>
                           <select name="doctor" class="create-visit-doctor" ${action === "update" ? "disabled" : null}>
                               <option value="" hidden disabled selected>Выбрать</option>
                               <option value="cardiologist">Кардиолог</option>
                               <option value="dentist">Стоматолог</option>
                               <option value="therapist">Tерапевт</option>
                           </select>
                       </div>
                         <div class="modal__item">
                             <label for="title">Цель визита:</label>
                             <input type="text" class="${action}-visit-title" id="update-visit-title" name="title" required>
                         </div>
               
                         <div class="modal__item">
                             <label for="description">Краткое описание визита:</label>
                             <input type="text" class="${action}-visit-description" id="update-visit-description" name="description" required>
                         </div>

                         <div  class="modal__item">
                           <label for="urgently">Выберите срочность</label>
                           <select name="urgently" class="${action}-visit-urgently" ${action === "update" ? "disabled" : null}>
                               <option value="" hidden disabled selected>Выбрать</option>
                               <option value="low">Обычная</option>
                               <option value="normal">Приоритетная</option>
                               <option value="high">Неотложная</option>
                           </select>
                       </div>
                         
                         <div class="modal__item">
                             <label for="surname">Фамилия:</label>
                             <input type="text" class="${action}-visit-surname" id="update-visit-surname" name="surname" required>
                         </div>
               
                         <div class="modal__item">
                             <label for="name">Имя:</label>
                             <input type="text" class="${action}-visit-name" id="update-visit-name" name="name" required>
                         </div>
               
                         <div class="modal__item">
                             <label for="patronim">Отчество:</label>
                             <input type="text" class="${action}-visit-patronim" id="update-visit-patronim" name="patronim" required>
                         </div>
                         <div class="place-to-render-inputs place-to-render-inputs-${action}-modal">
                        
                        </div>
                         <button class="${action}-visit-submit-button form-button card-button btn btn-success btn-sm">${submitBtnText}</button>
                       </form>
                     </div>
                   </div>
                </div>
             </div>`
        }
    }
};

class Visit {
    constructor(params) {
        this.id = params.id
        this.doctor = params.doctor;
        this.title = params.title;
        this.description = params.description;
        this.urgently = params.urgently;
        this.surname = params.surname;
        this.name = params.name;
        this.patronim = params.patronim;
        this.status = params.status;
    }

    getAllParentPropsInObj() {
        return {
            doctor: this.doctor,
            title: this.title,
            description: this.description,
            surname: this.surname,
            name: this.name,
            patronim: this.patronim,
            status: this.status,
            urgently: this.urgently
        }
    }

    render(){
        const id = this.id
        const name = this.name;
        const surname = this.surname;
        const patronim = this. patronim;
        const doctor = this.doctor;
        const status = this.status;
       return `
       <div class="card " id=${id}>
          <div class="card-body" >


             <div class="card-info">
                <p class="card-text card-visitor-details">ФИО: <span class="card-visitor-details"><b>${surname} ${name} ${patronim}</b></span></p>
                <p class="card-text card-doctor">Врач: <span class="card-doctor"><b>${doctor}</b></span></p>
                <p class="card-text card-status">Статус: <span class="card-status"><b>${status}</b></span></p>

             </div>
             <a href="#" class="delete-card-button">
                <i class="fas fa-trash trash-icon"></i>
             </a>
          </div>
          <div class="card-buttons">
             <button class="edit-card-button card-button btn btn-primary btn-sm" data-bs-toggle="modal"
                data-bs-target="#updateVisitModal">Редактировать</button>
             <button class="show-more-button card-button btn btn-primary btn-sm" data-bs-toggle="modal"
                data-bs-target="#showMoreVisitModal">Показать больше</button>
             <button class="change-status-button card-button btn btn-success btn-sm">Пометить как
                выполненное</button>
          </div>
       </div>
       `
    }
};

export {
    Visit,
    Modal
}