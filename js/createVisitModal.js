import { Modal } from './mainClasses.js';
import { $, elems } from './DOMElems.js';
import { BASE_URL } from './constants.js';
import { closeModal } from './helpers.js';

const getErrorMsg = (msg) => {
    return `
    <div class="alert alert-danger auth-modal-error" role="alert">
        ${msg}
    </div>
    `
}

const createNewVisit = (params) => {
    const token = localStorage.getItem('userToken');

    try {
        fetch(`${BASE_URL}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(params)
          })
        .then((res) => {
            if(res.status === 200) {
                return res.json();
            }
            else {
                throw new Error('Ответ сети был не ok.');
            }
        })
        .then((res) => {
            closeModal($(elems.createNewVisit.createNewVisitModal));
            window.location.reload(false); 
            alert("Вы успешно создали визит");
        })
        .catch(err => {
            $(elems.createNewVisit.modalBody).insertAdjacentHTML('afterbegin', getErrorMsg(err.message));
        });

    }
    catch(err) {
        console.log(err);
    };
}

const onClickSubmitButton = (e) => {
    e.preventDefault();

    const params = {};
    const errors = [];

    params.doctor = $(elems.createNewVisit.doctorInput).value ? $(elems.createNewVisit.doctorInput).value : errors.push('doctor');
    params.title = $(elems.createNewVisit.titleInput).value ? $(elems.createNewVisit.titleInput).value : errors.push('title');
    params.urgently = $(elems.createNewVisit.urgentlyInput).value ? $(elems.createNewVisit.urgentlyInput).value : errors.push('urgently');
    params.description = $(elems.createNewVisit.descriptionInput).value ? $(elems.createNewVisit.descriptionInput).value : errors.push('desc');
    params.surname = $(elems.createNewVisit.surnameInput).value ? $(elems.createNewVisit.surnameInput).value : errors.push('surname');
    params.name = $(elems.createNewVisit.nameInput).value ? $(elems.createNewVisit.nameInput).value : errors.push('name');
    params.patronim = $(elems.createNewVisit.patronimInput).value ? $(elems.createNewVisit.patronimInput).value : errors.push('patronim');
    
    switch(params.doctor) {
        case 'cardiologist':
            params.pressure = $(elems.createNewVisit.pressureInput).value ? $(elems.createNewVisit.pressureInput).value : errors.push('pressure');
            params.bmi = $(elems.createNewVisit.bmiInput).value ? $(elems.createNewVisit.bmiInput).value : errors.push('bmi');
            params.previousDiseases = $(elems.createNewVisit.previousDiseasesInput).value;
            params.age = $(elems.createNewVisit.ageInput).value ? $(elems.createNewVisit.ageInput).value : errors.push('age');
            break;
        case 'dentist':
            params.lastVisitDate = $(elems.createNewVisit.lastVisitDateInput).value ? $(elems.createNewVisit.lastVisitDateInput).value : errors.push('lastvisit');
            break;
        case 'therapist':
            params.age = $(elems.createNewVisit.ageInput).value ? $(elems.createNewVisit.ageInput).value : errors.push('age');
    }

    if(errors.length !== 0) {
        $(elems.createNewVisit.modalBody).insertAdjacentHTML('afterbegin', getErrorMsg("Please fill all the fields"));
    }
    else {
        params.status = "open";
        createNewVisit(params);
    }
}

const addInputs = {
    cardiologistInputs: `
    <div class="modal__item">
        <label for="pressure">Обычное давление:</label>
        <input type="text" class="create-visit-pressure" id="create-visit-pressure" name="pressure" required>
    </div>

    <div class="modal__item">
        <label for="bmi">Индекс массы тела:</label>
        <input type="text" class="create-visit-bmi" id="create-visit-bmi" name="bmi" required>
    </div>

    <div class="modal__item">
        <label for="previous-diseases">Перенесенные заболевания сердечно-сосудистой системы:</label>
        <input type="text" class="create-visit-previous-diseases" id="create-visit-previous-diseases" name="previous-diseases">
    </div>

    <div class="modal__item">
        <label for="age">Возраст:</label>
        <input type="number" class="create-visit-age" id="create-visit-age" name="age" required>
    </div>`,
    dentistInput: `
    <div class="modal__item">
        <label for="last-visit-date">Дата последнего визита:</label>
        <input type="date" class="create-visit-last-visit-date" id="create-visit-last-visit-date" name="last-visit-date" required>
    </div>`,
    therapistInput: `
    <div class="modal__item">
      <label for="age">Возраст:</label>
      <input type="number" class="create-visit-age" id="create-visit-age" name="age" required>
    </div>
    `
}

const renderInputs = (doctorType) => {
    $(elems.createNewVisit.placeToRenderInputs).innerHTML = '';
    let inputsToInsert;

    switch(doctorType) {
        case 'cardiologist':
            inputsToInsert = addInputs.cardiologistInputs;
            break;
        case 'dentist':
            inputsToInsert = addInputs.dentistInput;
            break;
        case 'therapist':
            inputsToInsert = addInputs.therapistInput;
    }

    $(elems.createNewVisit.placeToRenderInputs).innerHTML = inputsToInsert;
    $(elems.createNewVisit.submitButton).disabled = false;
}

const onChangeDoctorHandler = (e) => {
    const selectedOption = e.target.value;

    renderInputs(selectedOption);
}

export const addCreateVisitModalToHTML = () => {
    const propsObj = {
        id: "createVisitModal",
        title: "Создать визит",
        type: "visitModal",
        action: "create",
        submitBtnText: "Создать визит"
    };

    const authModal = new Modal(propsObj);

    $(elems.main).insertAdjacentHTML('beforebegin', authModal.getHtmlToInsert());

    $(elems.createNewVisit.doctorInput).addEventListener('change', (e) => onChangeDoctorHandler(e))

    $(elems.createNewVisit.submitButton).addEventListener('click', (e) => onClickSubmitButton(e));

}
