export const $ = (i) => document.querySelector(i); 

export const elems = {
    main: '.cards-list',
    header: {
        signInButton: '.header-button__exit',
        createNewVisitButton: '.header-button__create-visit'
    },
    auth: {
        authModal: '#authModal',
        errorMessage: '.auth-modal-error',
        modalBody: '.modal-body-auth',
        authEmailInput: '.auth-email-input',
        authPasswordInput: '.auth-password-input',
        submitButton: '.auth-form-submit-button',
    },
    searchArea: {
        searchInput: '.search-input',
        statusSelector: '.select-status',
        urgentlySelector: '.select-urgently',
        searchButton: '.search-area-button'
    },
    createNewVisit: {
        createNewVisitModal: '#createVisitModal',
        modalBody: '.modal-body-create',
        doctorInput: '.create-visit-doctor',
        titleInput: '.create-visit-title',
        descriptionInput: '.create-visit-description',
        urgentlyInput: '.create-visit-urgently',
        surnameInput: '.create-visit-surname',
        nameInput: '.create-visit-name',
        patronimInput: '.create-visit-patronim',
        pressureInput: '.create-visit-pressure',
        bmiInput: '.create-visit-bmi',
        previousDiseasesInput: '.create-visit-previous-diseases',
        ageInput: '.create-visit-age',
        lastVisitDateInput: '.create-visit-last-visit-date',
        placeToRenderInputs: '.place-to-render-inputs-create-modal',
        submitButton: '.create-visit-submit-button'
    },
    updateVisit: {
        updateVisitModal: '#updateVisitModal',
        modalBody: '.modal-body-update',
        doctorInput: '.update-visit-doctor',
        titleInput: '.update-visit-title',
        descriptionInput: '.update-visit-description',
        urgentlyInput: '.update-visit-urgently',
        surnameInput: '.update-visit-surname',
        nameInput: '.update-visit-name',
        patronimInput: '.update-visit-patronim',
        pressureInput: '.update-visit-pressure-visit',
        bmiInput: '.update-visit-bmi',
        previousDiseasesInput: '.update-visit-previous-diseases',
        ageInput: '.update-visit-age',
        lastVisitDateInput: '.update-visit-last-visit-date',
        placeToRenderInputs: '.place-to-render-inputs-update-modal',
        submitButton: '.update-visit-submit-button'
    },
    visitDetails: {
        doctor: '.doctor-visit',
        title: '.title-visit',
        description: '.description-visit',
        urgentlyInput: '.visit-urgently',
        surname: '.surname-visit',
        name: '.name-visit',
        patronim: '.patronim-visit',
        pressure: '.pressure-visit',
        bmi: '.bmi-visit',
        previousDiseases: '.previous-diseases-visit',
        age: '.age-visit',
        placeToRenderInputs: '.place-to-render-inputs-info-modal',
        lastVisitDate: '.last-visit-date-visit',
    },
    card: {
        details: '.card-visitor-details',
        doctor: '.card-doctor',
        status: '.card-status',
        deleteCardButton: '.delete-card-button',
        editCardButton: '.edit-card-button',
        showMoreCardButton: '.show-more-button',
        changeStatusButton: '.change-status-button'
    }
}