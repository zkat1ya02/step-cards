import { Visit } from './mainClasses.js';

class VisitCardiologist extends Visit {
  constructor(params){
      super(params);
       this.pressure = params.pressure;
       this.bmi = params.bmi;
       this.previousDiseases = params.previousDiseases;
       this.age = params.age;
   }

   getAllPropsInObj() {
       const parentProps = this.getAllParentPropsInObj();
       return {
           ...parentProps,
           pressure: this.pressure,
           bmi: this.bmi,
           previousDiseases: this.previousDiseases,
           age: this.age
       }
   }
   renderCardiologist(){

   }
}

class VisitTherapist extends Visit {
   constructor(params){
      super(params);
       this.age = params.age;
   }

   getAllPropsInObj() {
       const parentProps = this.getAllParentPropsInObj();
       return {
           ...parentProps,
           age: this.age
       }
   }
   renderTherapist(){

   }
}

class VisitDentist extends Visit {
   constructor(params){
      super(params);
       this.lastVisitDate = params.lastVisitDate;
   }

   getAllPropsInObj() {
     
       const parentProps = this.getAllParentPropsInObj();
       return {
           ...parentProps,
           lastVisitDate: this.lastVisitDate
       }
   }
   renderDentist(){
      const parentRender = this.render();
      return {
         parentRender
      }   
   }

   }

export {
   VisitCardiologist,
   VisitDentist,
   VisitTherapist
}