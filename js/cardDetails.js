import { Modal } from './mainClasses.js';
import { $, elems } from './DOMElems.js';
import { BASE_URL } from './constants.js';
import { closeModal } from './helpers.js';

export const showDetails = () => {
    document.body.addEventListener('click', function (event) {
        if (event.target.classList.contains('show-more-button')) {
            $(elems.visitDetails.placeToRenderInputs).innerHTML = "";
            const token = localStorage.getItem('userToken');
            const card = event.target.parentNode.parentNode;
            const cardId = card.id;
            try{
                fetch (`${BASE_URL}/${cardId}`,{
                    method: 'GET',
                        headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then((res) =>{
                    if (res.status ===200){
                        return res.json();
                    }
                    else{
                        throw new Error("Некорректный ответ");
                    }
                })

                .then(res =>{
                    // const modalBody = document.querySelector(".modal-body");
                    $(elems.visitDetails.doctor).innerHTML = res.doctor;
                    $(elems.visitDetails.title).innerHTML = res.title;
                    $(elems.visitDetails.description).innerHTML = res.description;
                    $(elems.visitDetails.surname).innerHTML = res.surname;
                    $(elems.visitDetails.name).innerHTML = res.name;
                    $(elems.visitDetails.patronim).innerHTML = res.patronim;

                    function createFields (fieldName){
                        if (fieldName){
                            const newField = document.createElement("p");
                            fieldName ===res.lastVisitDate ? newField.innerHTML = `Последний визит: ${fieldName}`:true;
                            fieldName ===res.urgently ? newField.innerHTML = `Срочность: ${fieldName}`:true;
                            fieldName ===res.pressure ? newField.innerHTML = `Давление: ${fieldName}`:true;
                            fieldName ===res.bmi ? newField.innerHTML = `Индекс массы тела: ${fieldName}`:true;
                            fieldName ===res.previousDiseases ? newField.innerHTML = `Перенесенные заболевания сердечно-сосудистой системы:: ${fieldName}`:true;
                            fieldName ===res.age ? newField.innerHTML = `Возраст: ${fieldName}`:true;
                            $(elems.visitDetails.placeToRenderInputs).append(newField);
                        }
                    }
                    createFields(res.lastVisitDate);
                    createFields(res.urgently);
                    createFields(res.pressure);
                    createFields(res.bmi);
                    createFields(res.previousDiseases);
                    createFields(res.age);
                })
            }
            catch (err) {
                console.log(err);
            }
        }
    })
}

