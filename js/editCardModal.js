import { Modal } from './mainClasses.js';
import { $, elems } from './DOMElems.js';
import { BASE_URL } from './constants.js';
import { closeModal } from './helpers.js';

export const editCard = () => {
    const token = localStorage.getItem('userToken');
    document.body.addEventListener('click', function (event) {
        if (event.target.classList.contains('edit-card-button')) {
            const editVisitDoctor = document.querySelector(".edit-visit-doctor");
            const card = event.target.parentNode.parentNode;
            const cardId = card.id;
            try{
                fetch (`${BASE_URL}/${cardId}`,{
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                })
                    .then((res) =>{
                        if (res.status ===200){
                            return res.json();
                        }
                        else{
                            throw new Error("Некорректный ответ");
                        }
                    })
                    .then(res =>{
                        console.log(res)


                        editVisitDoctor.value = res.doctor;
                        $(elems.updateVisit.titleInput).value = res.title;
                        $(elems.updateVisit.descriptionInput).value = res.description;
                        $(elems.updateVisit.surnameInput).value = res.surname;
                        $(elems.updateVisit.nameInput).value = res.name;
                        $(elems.updateVisit.patronimInput).value = res.patronim;
                        $(elems.updateVisit.urgentlyInput).value = res.urgently;



                        const addInputs = {
                            cardiologistInputs: `
                                <div class="modal__item">
                                    <label for="pressure">Обычное давление:</label>
                                    <input type="text" class="update-visit-pressure-visit" id="update-visit-pressure" name="pressure" required>
                                </div>

                                <div class="modal__item">
                                    <label for="bmi">Индекс массы тела:</label>
                                    <input type="text" class="update-visit-bmi" id="update-visit-bmi" name="bmi" required>
                                </div>

                                <div class="modal__item">
                                    <label for="previous-diseases">Перенесенные заболевания сердечно-сосудистой системы:</label>
                                    <input type="text" class="update-visit-previous-diseases" id="update-visit-previous-diseases" name="previous-diseases">
                                </div>
                            
                                <div class="modal__item">
                                    <label for="age">Возраст:</label>
                                    <input type="number" class="update-visit-age" id="update-visit-age" name="age" required>
                                </div>`,
                            dentistInput: `
                                <div class="modal__item">
                                    <label for="last-visit-date">Дата последнего визита:</label>
                                    <input type="date" class="update-visit-last-visit-date" id="update-visit-last-visit-date" name="last-visit-date" required>
                                </div>`,
                            therapistInput: `
                            <div class="modal__item">
                              <label for="age">Возраст:</label>
                              <input type="number" class="update-visit-age" id="update-visit-age" name="age" required>
                            </div>
                            `
                        }

                        const renderInputs = (doctorType) => {
                            $(elems.updateVisit.placeToRenderInputs).innerHTML = '';
                            let inputsToInsert;

                            switch(doctorType) {
                                case 'cardiologist':
                                    inputsToInsert = addInputs.cardiologistInputs;
                                    break;
                                case 'dentist':
                                    inputsToInsert = addInputs.dentistInput;
                                    break;
                                case 'therapist':
                                    inputsToInsert = addInputs.therapistInput;
                            }

                            $(elems.updateVisit.placeToRenderInputs).innerHTML = inputsToInsert;

                        }

                        renderInputs(editVisitDoctor.value);

                        const onChangeDoctorHandler = (e) => {
                            const selectedOption = e.target.value;

                            renderInputs(selectedOption);
                        }
                        editVisitDoctor.addEventListener('change', (e) => onChangeDoctorHandler(e));

                        switch(editVisitDoctor.value) {
                            case 'cardiologist':
                                $(elems.updateVisit.pressureInput).value = res.pressure;
                                $(elems.updateVisit.bmiInput).value = res.bmi;
                                $(elems.updateVisit.previousDiseasesInput).value = res.previousDiseases;
                                $(elems.updateVisit.ageInput).value = res.age;
                                break;
                            case 'dentist':
                                $(elems.updateVisit.lastVisitDateInput).value = res.lastVisitDate;
                                break;
                            case 'therapist':
                                $(elems.updateVisit.ageInput).value = res.age;
                        }
                    })
            }
            catch (err) {
                console.log(err);
            }
            document.body.addEventListener('click', function (event) {
                event.preventDefault()
                if (event.target.classList.contains('update-visit-submit-button')) {

                    fetch (`${BASE_URL}/${cardId}`,{
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },

                        body: JSON.stringify({
                            age: editVisitDoctor.value === "cardiologist" || editVisitDoctor.value === "therapist" ? $(elems.updateVisit.ageInput).value:false,
                            bmi: editVisitDoctor.value === "cardiologist" ? $(elems.updateVisit.bmiInput).value: false,
                            description: $(elems.updateVisit.descriptionInput).value,
                            doctor: editVisitDoctor.value,
                            name: $(elems.updateVisit.nameInput).value,
                            patronim: $(elems.updateVisit.patronimInput).value,
                            pressure: editVisitDoctor.value === "cardiologist" ? $(elems.updateVisit.pressureInput).value: false,
                            previousDiseases: editVisitDoctor.value === "cardiologist" ? $(elems.updateVisit.previousDiseasesInput).value: false,
                            surname: $(elems.updateVisit.surnameInput).value,
                            title: $(elems.updateVisit.titleInput).value,
                            urgently: $(elems.updateVisit.urgentlyInput).value,
                            status: "open",

                        })

                    })
                        .then(response => response.json())
                        .then(response => {
                            window.location.reload(false);
                            console.log(response)
                        })


                }
            })
        }
    })
}
