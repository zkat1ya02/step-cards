import { $, elems } from './DOMElems.js';


const beDeleted = () => {
   if (!confirm("Удалить запись на прийом к врачу?")) {
      return
   }
   return true
}
const deleteCard = () => {
   document.body.addEventListener('click', function (event) {
      if (event.target.classList.contains('trash-icon')) {
         if (beDeleted()) {
            const token = localStorage.getItem('userToken');
            const card = event.target.parentNode.parentNode.parentNode;
            console.log(card)
            const cardId = card.id
            try{
            fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
               method: 'DELETE',
               headers: {
                  'Authorization': `Bearer ${token}`
               },
            })
            .then((res) =>{
               if (res.status ===200){
                  window.location.reload(false);
                }
                else{
                   throw new Error("Не корректный ответ");
                }
            })
         }
         catch(err){
            console.log(err);
         }
         }
      }
   })
}

export {
   deleteCard
}