## A simple CRUD e-medicine app written on vanilla JS

Authors and responsibilities:

Html template, bootstrap based ui, children classes(VisitModal, VisitCardiologist, VisitTherapist), card(basic), change visit status button logic, delete button logic, list of cards - Kateryna Zubenko.

Header,  switch buttons(Login/Create visit) logic, filtering cards logic, card(in modal window with details), update card modal logic - Dmytro Minchenko.

Main classes(Modal, Visit),  authentication modal logic, create new visit modal logic - Anastasia Zastela.
